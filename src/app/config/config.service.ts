import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { Observable } from 'rxjs/Observable';

const RSS_NEWS_FEED = 'https://feeds.bbci.co.uk/news/technology/rss.xml?edition=uk';
// const RSS_NEWS_FEED = 'https://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml';

const configData = {
  "modules" : [ {
    "active" : false,
    "header" : "Messages",
    "module" : "message",
    "position" : "lower_third"
  },{
    "active" : false,
    "header" : "Compliments",
    "module" : "compliments",
    "position" : "lower_third"
  }, {
    "active" : true,
    "config" : {
      "calendars" : [ {
        "symbol" : "calendar-check-o ",
        "url" : "webcal://www.calendarlabs.com/ical-calendar/ics/196/FIFA_World_Cup_2018.ics"
      } ]
    },
    "header" : "US Holidays",
    "module" : "calendar",
    "position" : "top_right"
  }, {
    "active" : false,
    "header" : "Alert",
    "module" : "alert"
  },
  {
    "active" : false,
    "header" : "Home",
    "module" : "home",
    "position" : "top_left"
  }, {
    "active" : false,
    "header" : "Clock",
    "module" : "clock",
    "position" : "top_center"
  }, {
    "active" : true,
    "config" : {
      "appid" : "ec4608cb1c1d22fbe047ae1517492b93",
      "location" : "Budapest, HU",
      "locationID" : ""
    },
    "header" : "Current Weather",
    "module" : "currentweather",
    "position" : "top_left"
  }, {
    "active" : false,
    "config" : {
      "appid" : "ec4608cb1c1d22fbe047ae1517492b93",
      "location" : "Budapest, HU",
      "locationID" : "5128581"
    },
    "header" : "Weather Forecast",
    "module" : "weatherforecast",
    "position" : "top_right"
  }, {
    "active" : false,
    "config" : {
      "api_key" : 12345,
      "search" : "ngconf",
      "twitter_consumer_key" : "I2x2Z584EZSdZWrLNSB69wwoG",
      "twitter_consumer_secret" : "2iNJO4DvWR6X7nLJ3MOJowd202NmcoVq8UUbVLggs03zOuOGc1"
    },
    "header" : "Tweet Line",
    "module" : "tweetline",
    "position" : "bottom_bar"
  }, {
    "active" : true,
    "config" : {
      "feeds" : [ {
        "title" : "BBC",
        "url" : RSS_NEWS_FEED
      } ],
      "showPublishDate" : true,
      "showSourceTitle" : true
    },
    "header" : "News Feed",
    "module" : "newsfeed",
    "position" : "bottom_bar"
  }, {
    "active" : true,
    "header" : "Date time",
    "module" : "datetime",
    "position" : "top_left"
  },
  {
    "active" : true,
    "header" : "Bubi Bikes",
    "module" : "bubi-bike",
    "position" : "top_left",
    "stations": [
       '35d28be43d0cfec1ce7b1064481295cd',
       'e46ca9a3c37ad658bdacb619ac82238b'
    ]
  } ],
  "user" : "KwUhRABnm1MWc90f3T5bJROYSmz2"
};

@Injectable()
export class ConfigService {

  conf: FirebaseListObservable<any>;
  name: any;
  msgVal: string = '';

  constructor(
    public http: Http,
    public af: AngularFire) {
     this.get(localStorage.getItem('remoteID')).subscribe( result => this.conf = result);
  }

  getConf(){
    return this.conf;
  }

  getSettings(parameter: string) {

    return this.conf.filter(item => {
      return item['module'] === parameter;
    });
    // return this.conf.map( modules => modules).filter(item => {
    //   return item['module'] === parameter;
    // })
  }

  getConfParameter(parameter): Observable<any> {
    const config = configData.modules.filter( modules => modules['module'] === parameter);
    console.log(config);
  return Observable.of(config[0]);
    // return this.conf.flatMap( modules => modules).filter(item => {
    //   return item['module'] === parameter;
    // })
  }

  get(remoteID: string): Observable<any> {
    return this.af.database.object('/' + remoteID + '/modules');
  }

  getKeys(){
    return this.conf;
  }

  getApi(name){
    return this.conf[name];
  }

  getKey(name){
    return this.conf[name].key;
  }

  getUrl(name){
    return this.conf[name].url;
  }

}
