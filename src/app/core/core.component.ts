import { Router, ActivatedRoute } from '@angular/router';
import { Component, ViewChild, ViewContainerRef, ComponentFactoryResolver, OnInit } from '@angular/core';

@Component({
  selector: 'core-component',
  template: `
  <div #topLeftContainer></div>
  `
})
export class MasterComponent implements OnInit {
   @ViewChild('topLeftContainer', { read: ViewContainerRef }) topLeftContainer: ViewContainerRef;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private _componentFactoryResolver: ComponentFactoryResolver,
        private viewContainer: ViewContainerRef) {

    }

    ngOnInit(){
       this.route.data
           .subscribe(data => {
              if (!!data && !!data.top_left && data.top_left.length > 0) {
                Object.keys(data).forEach(key => {
                  data[key].map(comp => {
                    const componentFactory = this._componentFactoryResolver.resolveComponentFactory(comp);
                    this.topLeftContainer.createComponent(componentFactory);
                  });
                });
              }
           });
    }

}
