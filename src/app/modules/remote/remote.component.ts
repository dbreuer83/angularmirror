
import { Component, OnChanges, SimpleChange, Input, ElementRef, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';

import * as QRCode from 'qrcodejs2';

function isValidQrCodeText(data: string) {
  return !(typeof data === 'undefined' || data === '');
}

/**
 * QR code
 * data {String} ''
 * size {Number} 128
 * level {String} 'M'
 * type {Number} 4
 *
 */
@Component({
  selector: 'mm-remote',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '',
  styles: [`
    mm-remote {
        position: absolute;
        width: 256px;
        height: 256px;
        top: 50%;
        left: 50%;
        margin-left: -128px;
        margin-top: -128px;
        border: 15px solid #fff;
    }
  `]
})
export class RemoteComponent implements OnChanges {

  @Input() qrdata: string = '';
  @Input() size: number = 256;
  @Input() level: string = 'M';
  @Input() colordark: string = '#000000';
  @Input() colorlight: string = '#ffffff';
  @Input() usesvg: boolean = false;
  @Input() showQRCode: boolean = true;
  private qrcode: any;

  constructor(private el: ElementRef) { }

  ngOnInit() {
    try {
      if (!isValidQrCodeText(this.qrdata)) {
        throw new Error('Empty QR Code data');
      }
      this.qrcode = new QRCode(this.el.nativeElement, {
        text: this.qrdata,
        width: this.size,
        height: this.size,
        colorDark: this.colordark,
        colorLight: this.colorlight,
        useSVG: this.usesvg,
        correctLevel: QRCode.CorrectLevel[this.level.toString()]
      });
    } catch (e) {
      console.error('Error generating QR Code: ' + e.message);
    }
  }

  ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
    if (!this.qrcode || this.showQRCode===false) {
      return;
    }
    const qrData = changes['qrdata'];
    if (qrData && isValidQrCodeText(qrData.currentValue)) {
      this.qrcode.clear();
      this.qrcode.makeCode(qrData.currentValue);
    }
  }
}
