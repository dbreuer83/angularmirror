import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RemoteComponent } from './remote.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [RemoteComponent],
  entryComponents: [RemoteComponent],
  exports: [RemoteComponent]
})
export class RemoteModule { }
