import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TwitterComponent } from './twitter.component';
import { TwitterService } from './twitter.service';
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    TwitterComponent
    ],
    exports: [
      TwitterComponent
    ],
    providers:[
      TwitterService
    ]
})
export class TwitterModule {

 }
