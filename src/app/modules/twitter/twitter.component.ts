import { ConfigService } from './../../config/config.service';
import { Component, OnInit, HostBinding } from '@angular/core';

@Component({
  selector: 'app-twitter',
  templateUrl: './twitter.component.html',
  styleUrls: ['./twitter.component.scss']
})
export class TwitterComponent implements OnInit {
  @HostBinding('class') modulePosition: string;
  public isActive = false;

  constructor(private configService: ConfigService) {
    configService.getConfParameter('twitter').subscribe((config: any) => {
      this.modulePosition = config.position;
      this.isActive = config.active;
    });
  }

  ngOnInit() {
  }

}
