import { ConfigService } from './../../config/config.service';
import { HostBinding } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'clock',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.scss']
})

export class ClockComponent implements OnInit {
  @HostBinding('class') modulePosition: string;
  public isActive = false;
  public _sRotate: string = '1';
  public _mRotate: string = '1';
  public _hRotate: string = '1';

  constructor(private configService: ConfigService) {
    configService.getConfParameter('clock').subscribe((config: any) => {
      this.modulePosition = config.position;
      this.isActive = config.active;
    });
  }

  ngOnInit() {
    console.log('Init Clock');
    setInterval(() => {
      this.clockUpdate();
    }, 1000);
  }

  clockUpdate() {
    var d = new Date();
    var minutes = d.getMinutes() * 6;
    var hours = d.getHours() % 12 / 12 * 360 + (minutes / 12);
    var secondes = d.getSeconds() * 6;
    this._sRotate = 'rotate(' + secondes + 'deg)';
    this._mRotate = 'rotate(' + minutes + 'deg)';
    this._hRotate = 'rotate(' + hours + 'deg)';
  }

}
