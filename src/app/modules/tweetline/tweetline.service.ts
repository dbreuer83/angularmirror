import { ConfigService } from './../../config/config.service';

import { Http, Headers, Jsonp, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { ConstantsService } from '../../api/constants.service';
import {Observable} from 'rxjs/Rx';

@Injectable()
export class TweetlineService {
  public api_url: String = 'https://api.twitter.com/';
  public oauth_url = 'oauth2/token';
  public search_url = '1.1/search/tweets.json?q=';
  private isActive = false;
  public http: any;

  public consumerkey: any;
  public consumersecret: any;
  public apikeyService: any;

  public token: any;
  constructor(http: Http, private constants: ConstantsService, private configService: ConfigService) {
     this.http = http;
     configService.getConfParameter('tweetline').subscribe((config: any) => {
       this.isActive = config['active'];
      this.constants.tweets_hashtag = config['config'].search;
      this.apikeyService = config['config'].api_key || '';
      this.consumerkey = config['config'].twitter_consumer_key || '';
      this.consumersecret = config['config'].twitter_consumer_secret || '';

    });


  }

  getAuthorization() {
    if (!this.isActive) {
      return Observable.throw('Disabled');
    }
    console.log('tweetline', this.isActive);
    let options = this.getTokenRequestOptions();
    let body = 'grant_type=client_credentials';
    return this.http.post(this.api_url + this.oauth_url, body, options)
      .map(
      function (res) {
        this.token = res.json().access_token;
        return res.json();
      }
      )
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getTweets(token: any) {
    var encsearchquery = encodeURIComponent(this.constants.tweets_hashtag);
    var bearerheader = 'Bearer ' + token.access_token;
    return this.http.get(this.api_url + this.search_url + encsearchquery +
      '&result_type=recent&count=1', { headers: { Authorization: bearerheader } })
      .map(
      function (res) {
        return res.json().statuses;
      })
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
;
  }

  private getTokenRequestOptions() {
    let header = this.consumerkey + ':' + this.consumersecret;
    let encheader = new Buffer(header).toString('base64');
    let finalheader = 'Basic ' + encheader;

    let headers = new Headers({ 'Authorization': finalheader });
    headers.append('content-type', 'application/x-www-form-urlencoded;charset=UTF-8');
    return new RequestOptions({ headers: headers });

  }

}
