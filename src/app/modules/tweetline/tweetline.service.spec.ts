/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { TweetlineService } from './tweetline.service';

describe('Service: Tweetline', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TweetlineService]
    });
  });

  it('should ...', inject([TweetlineService], (service: TweetlineService) => {
    expect(service).toBeTruthy();
  }));
});
