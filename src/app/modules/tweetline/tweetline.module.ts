
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TweetlineComponent } from './tweetline.component';
import { TweetlineService } from './tweetline.service';
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    TweetlineComponent
  ],
  providers: [TweetlineService],
  exports: [
    TweetlineComponent
  ]
})
export class TweetlineModule { }
