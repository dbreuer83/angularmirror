import { ConfigService } from './../../config/config.service';

import { Component, OnInit, ViewEncapsulation, HostBinding } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { TweetlineService } from './tweetline.service';
import { ConstantsService } from './../../api/constants.service';

interface TwitterConfig {
  twitter_consumer_key: string;
  twitter_consumer_secret: string;
}
@Component({
  selector: 'mm-tweetline',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './tweetline.component.html',
  styleUrls: ['./tweetline.component.scss']
})
export class TweetlineComponent implements OnInit {
  @HostBinding('class') modulePosition: string;
  public isActive = false;

  public tweets: any[] = [];
  private className: String;
  private token: any;
  private isToFadeOut = false;
  private app: TwitterConfig;
  constructor(private tweetlineService: TweetlineService, private constants: ConstantsService, private configService: ConfigService) {
    configService.getConfParameter('tweetline').subscribe((config: any) => {
      this.modulePosition = config.position;
      this.isActive = config.active;
      this.app = {
        twitter_consumer_key: config['config']['twitter_consumer_key'],
        twitter_consumer_secret: config['config']['twitter_consumer_secret']
      };

      if (this.app['twitter_consumer_key'] && this.app['twitter_consumer_secret'] && config.active) {
        this.initTweetline();
      }
    });
  }


  ngOnInit() {
    console.log('Init tweetline');
    if (!this.app.twitter_consumer_key || !this.app.twitter_consumer_secret || !this.isActive) {
      return false;
    }
    console.log('Get tweetline');
    this.initTweetline()
  }

  initTweetline() {

    Observable
          .from(this.tweetlineService.getAuthorization())
          .subscribe((o: any) => {
            this.token = o;
            this.getTweets();
            this.getTweetsByInterval(this.token);
            console.log('Tweet token: ', o);
          });
  }

  private getTweetsByInterval(jsonResponse: any) {
    setInterval(() => {
      this.getTweets();
    }, this.constants.tweets_refresh_interval);
  }

  private getTweets() {
    Observable
      .from(this.tweetlineService.getTweets(this.token))
      .map((res: any) => res)
      .flatMap((res: any) => res)
      .subscribe((lastTweet: any) => {
        this.publishLastTweet(lastTweet);
      });
  }

  private publishLastTweet(lastTweet: any) {
    var trouve = false;
    for (var tweet of this.tweets) {
      if (tweet.id == lastTweet.id) {
        trouve = true;
      }
    }
    if (!trouve) {
      this.isToFadeOut = true;
      setTimeout(() => {
        this.isToFadeOut = false;
        this.tweets = [];
        this.tweets.push(lastTweet);
      }, 1000);
    }

  }

}
