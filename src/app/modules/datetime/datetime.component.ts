import { ConfigService } from './../../config/config.service';
import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation, HostBinding } from '@angular/core';

@Component({
  selector: 'datetime',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './datetime.component.html',
  styleUrls: ['./datetime.component.scss']
})
export class DateTimeComponent implements OnInit {
  @HostBinding('class') modulePosition: string;
  public isActive = false;
  public currentDate: Date;

  constructor(private configService: ConfigService) {
    configService.getConfParameter('datetime').subscribe((config: any) => {
      this.modulePosition = config.position;
      this.isActive = config.active;
      this.loopDate();
    });
  }

  ngOnInit() {
    console.log('Init DateTime');
    setInterval(() => {
      this.loopDate();
    }, 10000);
  }

  loopDate() {
    this.currentDate = new Date();
  }
}
