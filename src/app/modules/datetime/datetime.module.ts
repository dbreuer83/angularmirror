import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateTimeComponent } from './datetime.component';
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    DateTimeComponent
    ],
    exports: [
      DateTimeComponent
    ]
})
export class DateTimeModule {

 }
