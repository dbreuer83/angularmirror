import { Observable } from 'rxjs';
import { ConfigService } from './../../config/config.service';
import { Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams } from '@angular/http';

@Injectable()
export class WeatherService {
  public settings: any;
  public apiForecast: string = "https://api.openweathermap.org/data/2.5/forecast";
  public apiCurrent: string = "https://api.openweathermap.org/data/2.5/weather";
  public key: string = "macle";
  public place: string;
  public key_url: string;
  public requestUrl: string;
  public headers: Headers;
  public params: URLSearchParams = new URLSearchParams();

  constructor(public http: Http, public configService: ConfigService) {

  }

  getCurrent(): Observable<any> {
    return this.configService.getConfParameter('currentweather').flatMap(module => {
      const config = module['config'];
      this.key = config['appid'] || '';
      this.place = config['location'] || '';
      this.params.set('APPID', this.key);
      this.params.set('q', this.place);
      this.params.set('units', 'metric');
      return this.http.get(this.apiCurrent, { search: this.params })
        .map(res => {
          const resp = Object.assign(res.json(), module);
          return resp;
        });
    });
  }

  getForecast(): Observable<any> {
   return this.configService.getConfParameter('weatherforecast').flatMap(module => {
      const config = module['config'];
      this.key = config['appid'] || '';
      this.place = config['location'] || '';
      this.params.set('APPID', this.key);
      this.params.set('q', this.place);
      this.params.set('units', 'metric');
      this.params.set('cnt', '6');
      return this.http.get(this.apiForecast, { search: this.params })
        .map(res => {
          const resp = Object.assign(res.json(), module);
          return resp;
        });
     });
  }
}
