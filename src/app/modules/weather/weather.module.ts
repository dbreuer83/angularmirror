import { MdIconModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeatherComponent } from './weather.component';
import { WeatherService } from './weather.service';
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule,
    MdIconModule
  ],
  declarations: [
    WeatherComponent
    ],
    exports: [
      WeatherComponent
    ],
    providers:[
      WeatherService
    ]
})
export class WeatherModule {

 }
