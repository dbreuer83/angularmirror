import { ConfigService } from './../../config/config.service';
import { Component, OnInit, HostBinding } from '@angular/core';

@Component({
  selector: 'social',
  templateUrl: './social.component.html',
  styleUrls: ['./social.component.scss']
})
export class SocialComponent implements OnInit {
  @HostBinding('class') modulePosition: string;
  public isActive = false;

  constructor(private configService: ConfigService) {
    configService.getConfParameter('social').subscribe((config: any) => {
      this.modulePosition = config.position;
      this.isActive = config.active;
    });
   }

  ngOnInit() {
  }

}
