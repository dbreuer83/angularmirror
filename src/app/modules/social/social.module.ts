import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SocialComponent } from './social.component';
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    SocialComponent
    ],
    exports: [
      SocialComponent
    ]
})
export class SocialModule {

 }
