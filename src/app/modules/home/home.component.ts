import { ConfigService } from './../../config/config.service';
import {Component, OnInit,ElementRef} from "@angular/core";
import {ViewEncapsulation, HostBinding} from '@angular/core';
import {NgStyle} from '@angular/common';

@Component({
  selector: 'home',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit{
  @HostBinding('class') modulePosition: string;
  public isActive = false;
  public _sRotate: string = "1";

  constructor(private configService: ConfigService) {
    configService.getConfParameter('home').subscribe((config: any) => {
      this.modulePosition = config.position;
      this.isActive = config.active;
    });
  }

  ngOnInit(){
    console.log('Init Home');

  }
}
