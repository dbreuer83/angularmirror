import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarComponent, OrderByPipe } from './calendar.component';
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    CalendarComponent,
    OrderByPipe
    ],
    exports: [
      CalendarComponent
    ]
})
export class CalendarModule {

 }
