import { ConfigService } from './../../config/config.service';
import { Component, OnInit, HostBinding, PipeTransform } from '@angular/core';
import { ViewEncapsulation, Pipe } from '@angular/core';
import { Http } from '@angular/http';

@Pipe({
  name: 'orderBy'
})
export class OrderByPipe implements PipeTransform {

  transform(value: any[], expression?: any, reverse?: boolean): any {
    if (!value) {
      return value;
    }

    let array: any[] = value.sort((a: any, b: any): number => {
      return a[expression] > b[expression] ? 1 : -1;
    });

    if (reverse) {
      return array.reverse();
    }

    return array;
  }
}


@Component({
  selector: 'calendar',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {
  @HostBinding('class') modulePosition: string;
  public isActive = false;
  public events: Array<any> = [];
  public months: Array<string> = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  public todayEvent: Array<any> = [];
  public yearlyEvent: any = {
    name: ''
  };
  public icalData = '';
  public calendarUrl: string;

  constructor(public http: Http, configService: ConfigService) {
    configService.getConfParameter('calendar').subscribe((config: any) => {
      this.calendarUrl = config['config'].calendars[0].url;
      this.modulePosition = config.position;
      this.isActive = config.active;
      this.getEvents();
    });
    //   webcal://www.calendarlabs.com/templates/ical/US-Holidays.ics
  }

  ngOnInit() {
    console.log('Init Calandar');
    setInterval(() => {
      this.getEvents();
    }, 1000 * 60 * 60 * 24);

  }

  getEvents() {
    if (!this.calendarUrl) {
      return false;
    }
    this.getCalendar()
      .subscribe((res) => {
        let today = new Date();
        this.events = res;
        this.yearlyEvent = this.events.filter((item) => {
          let startMonth = new Date(item.startDate).getMonth();
          let startDay = new Date(item.startDate).getDate();

          // item.allday = ((item.startDate - item.endDate) / (3600 * 1000 * 24) % 1 === 0);
          if ((item.type === 'VEVENT' && item.rrule === 'FREQ=YEARLY' && startDay === today.getDate() && startMonth === today.getMonth() )) {
            console.log(startDay, startMonth, today.getDate(), today.getMonth())
          }
          return (item.type === 'VEVENT' && item.rrule === 'FREQ=YEARLY' && startDay === today.getDate() && startMonth === today.getMonth() );
        })[0];
        this.events = res.filter((item) => {
          let start = new Date(item.startDate);
          item.allday = ((item.startDate - item.endDate) / (3600 * 1000 * 24) % 1 === 0);
          item.moreThanOne = ((item.startDate - item.endDate) / (3600 * 1000 * 24) % 1 === 0)
          ? ((item.startDate - item.endDate) / (3600 * 1000 * 24) > 1) : false;
          return (item.type === 'VEVENT' && start > today);
        });
        this.todayEvent = this.events.filter((item) => {
          let start = new Date(item.startDate);
          item.allday = ((item.startDate - item.endDate) / (3600 * 1000 * 24) % 1 === 0);
          return (item.type === 'VEVENT' && start.toDateString() === today.toDateString());
        });

      });
  }

  getCalendar() {

    this.calendarUrl = this.calendarUrl.replace('webcal://', 'https://');
    return this.http.get(this.calendarUrl)
      .map(res => this.parseICS(res.text()));
  }

  generateDateFunction(name: string) {
    return function (value, params, events, lastEvent) {
      let matches = /^(\d{4})(\d{2})(\d{2})$/.exec(value);
      if (matches !== null) {
        lastEvent[name] = new Date(parseInt(matches[1], 10), parseInt(matches[2], 10) - 1, parseInt(matches[3], 10)).getTime();
        return lastEvent;
      }

      if (/^(\d{4})(\d{2})(\d{2})T(\d{2})(\d{2})(\d{2})/.test(value)) {
        lastEvent[name] = new Date(value.substring(0, 4) + '-' + value.substring(4, 6) + '-' + value.substring(6, 11) + ':' + value.substring(11, 13) + ':' + value.substring(13)).getTime();
      }
      return lastEvent;
    };
  }

  generateSimpleParamFunction(name: string) {
    return function (value, params, events, lastEvent) {
      lastEvent[name] = value.replace(/\\n/g, "\n");
      return lastEvent;
    };
  }

  public objects = {
    'BEGIN': function objectBegin(value, params, events, lastEvent) {
      if (value === "VCALENDAR") {
        return null;
      }

      lastEvent = {
        type: value
      };
      events.push(lastEvent);

      return lastEvent;
    },

    'END': function objectEnd(value, params, events, lastEvent, data) {
      if (value === "VCALENDAR") {
        return lastEvent;
      }

      data.push(lastEvent);

      let index = events.indexOf(lastEvent);
      if (index !== -1) {
        events.splice(events.indexOf(lastEvent), 1);
      }

      if (events.length === 0) {
        lastEvent = null;
      }
      else {
        lastEvent = events[events.length - 1];
      }

      return lastEvent;
    },

    'DTSTART': this.generateDateFunction('startDate'),
    'DTEND': this.generateDateFunction('endDate'),
    'DTSTAMP': this.generateDateFunction('end'),
    'COMPLETED': this.generateDateFunction('completed'),
    'DUE': this.generateDateFunction('due'),
    'RRULE': this.generateSimpleParamFunction('rrule'),
    'UID': this.generateSimpleParamFunction('uid'),
    'SUMMARY': this.generateSimpleParamFunction('name'),
    'DESCRIPTION': this.generateSimpleParamFunction('description'),
    'LOCATION': this.generateSimpleParamFunction('location'),
    'URL': this.generateSimpleParamFunction('url'),

    'ORGANIZER': function objectOrganizer(value, params, events, lastEvent) {
      let mail = value.replace(/MAILTO:/i, '');

      if (params.CN) {
        lastEvent.organizer = {
          name: params.CN,
          mail: mail
        };
      }
      else {
        lastEvent.organizer = {
          mail: mail
        };
      }
      return lastEvent;
    },

    'GEO': function objectGeo(value, params, events, lastEvent) {
      let pos = value.split(';');
      if (pos.length !== 2) {
        return lastEvent;
      }

      lastEvent.geo = {};
      lastEvent.geo.latitude = Number(pos[0]);
      lastEvent.geo.longitude = Number(pos[1]);
      return lastEvent;
    },

    'CATEGORIES': function objectCategories(value, params, events, lastEvent) {
      lastEvent.categories = value.split(/\s*,\s*/g);
      return lastEvent;
    },

    'ATTENDEE': function objectAttendee(value, params, events, lastEvent) {
      if (!lastEvent.attendee) {
        lastEvent.attendee = [];
      }

      let mail = value.replace(/MAILTO:/i, '');

      if (params.CN) {
        lastEvent.attendee.push({
          name: params.CN,
          mail: mail
        });
      }
      else {
        lastEvent.attendee.push({
          mail: mail
        });
      }
      return lastEvent;
    }
  };

  parseICS(str: string) {
    let data = [];

    let events = [];
    let lastEvent = {};

    let lines = str.split('\n');

    for (let i = 0, len = lines.length; i < len; i += 1) {
      let line = lines[i].trim();

      while (i + 1 < len && lines[i + 1].match(/^ /)) {
        i += 1;
        line += lines[i].trim();
      }

      let dataLine = line.split(':');
      if (dataLine.length < 2) {
        continue;
      }

      let dataName = dataLine[0].split(';');

      let name = dataName[0];
      dataName.splice(0, 1);

      let params = {};
      dataName.forEach(function (param) {
        let Currentparam: Array<string> = param.split('=');
        if (param.length === 2) {
          params[param[0]] = param[1];
        }
      });

      dataLine.splice(0, 1);
      let value = dataLine.join(':').replace('\\,', ',');
      if (this.objects[name]) {
        lastEvent = this.objects[name](value, (params) ? params : '', events, (lastEvent) ? lastEvent : {}, data);
      }
    }

    return data;
  };

}
