import { ConfigService } from './../../config/config.service';
import { Component, OnInit, HostBinding } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'message',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})

export class MessageComponent implements OnInit {
  @HostBinding('class') modulePosition: string;
  public isActive = false;
  public greeting: string;
  public username: string;

  constructor(public configService: ConfigService) {
    this.greeting = 'Good morning';
    configService.getConfParameter('message').subscribe((config: any) => {
      this.username = config['username'] || 'Guest';
      this.modulePosition = config.position;
      this.isActive = config.active;
    });
  }

  ngOnInit() {
    console.log('Init Message');
    this.setGreetingByTime();
  }

  setGreetingByTime(): void {
    let time = new Date().getHours();
    if (time < 10) {
      this.greeting = 'Good morning';
    } else if (time < 20) {
      this.greeting = 'Good day';
    } else {
      this.greeting = 'Good evening';
    }
  }

  setUserName(name: string): void {
    this.username = name;
  }
}
