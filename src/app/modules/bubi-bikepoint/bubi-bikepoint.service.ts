import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

@Injectable()
export class BubiBikepointService {
  //view-source:http://vlille.fr/stations/xml-stations.aspx
  //http://vlille.fr/stations/xml-station.aspx?borne=10 //station rihour
  //http://vlille.fr/stations/xml-station.aspx?borne=36 //station cormontaigne

  //https://api.tfl.gov.uk/BikePoint/
  //https://api.tfl.gov.uk/BikePoint/Search?query=Barbican
  //https://api.tfl.gov.uk/BikePoint/BikePoints_275 //Barbican Centre, Barbican

  public api_url: string = "https://api.citybik.es/v2/networks/bubi";

  constructor(public http: Http) {}

  getBikeData(BikePoint?: {id: string, name: string}) {
    console.log(BikePoint);
    return this.http
      .get(this.api_url).map(res=> res.json().network.stations).mergeMap(stations => stations.filter( (station: any) => {
        return (station) ? station.id === BikePoint.id : true;
      }));
  }
}
