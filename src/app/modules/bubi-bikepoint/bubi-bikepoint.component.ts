import { ConfigService } from './../../config/config.service';
import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation, HostBinding } from '@angular/core';
import { BubiBikepointService } from './bubi-bikepoint.service';
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'bubi-bikepoint',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './bubi-bikepoint.component.html',
  styleUrls: ['./bubi-bikepoint.component.scss']
})

export class BubiBikepointComponent implements OnInit {
  @HostBinding('class') modulePosition: string;
  public isActive = false;

  public stationids: any[] = [];
  public stations: any[] = [];
  public interval: number;

  constructor(private bubibikepointService: BubiBikepointService, private configService: ConfigService) {
    configService.getConfParameter('bubi-bike').subscribe((config: any) => {
      this.modulePosition = config.position;
      this.isActive = config.active;
      this.stationids = Object.keys(config.stations).map((k) => {
        return { name: k, id: config.stations[k] }
      });
    });
    //  this.configService.getConfParameter('bubi-bike').subscribe( data => {
    //   const stationIDs = (data['bubi-bike'] && data['bubi-bike']['stations']) ? data['bubi-bike']['stations'] : [];
    //   if (data['bubi-bike'] && data['bubi-bike']['stations']) {
    //     this.stationids = Object.keys(stationIDs).map((k) => {
    //       return {name: k, id: stationIDs[k]}
    //     });
    //   }
    // });
    // this.stationids = [
    //   {name: 'Limehouse', id: 'BikePoints_542'},
    //   {name: 'Barbican', id: 'BikePoints_275'}
    // ];
    this.interval = 60000; // secondes
  }


  ngOnInit() {
    console.log('Init BUBI BIKEPOINT');

    this.getBornesData();

    setInterval(() => {
      this.getBornesData();
    }, this.interval);
  }

  getBornesData() {
    this.stations = [];
    if (this.stationids) {
      Observable
        .from(this.stationids)
        .flatMap((i) => this.bubibikepointService.getBikeData(i))
        .subscribe((o: any) => {
          console.log(o);
          const stationObject: any = {
            name: o.name,
            bikes: o.free_bikes,
            attachs: o.empty_slots
          };
          this.stations.push(stationObject);
        });
    }
  }

}
