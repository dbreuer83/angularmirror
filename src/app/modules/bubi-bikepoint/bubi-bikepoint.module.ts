import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BubiBikepointComponent } from './bubi-bikepoint.component';
import { BubiBikepointService } from './bubi-bikepoint.service';
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    BubiBikepointComponent
    ],
    exports: [
      BubiBikepointComponent
    ],
    providers: [
      BubiBikepointService
    ]
})
export class BubiBikepointModule {

 }
