import {Injectable} from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class TflBikepointService {

  //view-source:http://vlille.fr/stations/xml-stations.aspx
  //http://vlille.fr/stations/xml-station.aspx?borne=10 //station rihour
  //http://vlille.fr/stations/xml-station.aspx?borne=36 //station cormontaigne

  //https://api.tfl.gov.uk/BikePoint/
  //https://api.tfl.gov.uk/BikePoint/Search?query=Barbican
  //https://api.tfl.gov.uk/BikePoint/BikePoints_275 //Barbican Centre, Barbican

  public api_url : String = "https://api.tfl.gov.uk/BikePoint/";

  constructor(public http : Http) {
  }

  getBikeData(BikePoint){
    return this.http
      .get(this.api_url+BikePoint.id)
      .map(res => res.json());
  }

}
