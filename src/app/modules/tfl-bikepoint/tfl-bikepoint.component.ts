import { ConfigService } from './../../config/config.service';
import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation, HostBinding } from '@angular/core';
import { TflBikepointService } from './tfl-bikepoint.service';
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'tfl-bikepoint',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './tfl-bikepoint.component.html',
  styleUrls: ['./tfl-bikepoint.component.scss']
})

export class TflBikepointComponent implements OnInit {
  @HostBinding('class') modulePosition: string;
  public isActive = false;

  public stationids: any[] = [];
  public stations: any[] = [];
  public interval: number;

  constructor(private tflbikepointService: TflBikepointService, private configService: ConfigService) {
    configService.getConfParameter('tfl-bike').subscribe((config: any) => {
      this.modulePosition = config.position;
      this.isActive = config.active;
      this.stationids = Object.keys(config.stations).map((k) => {
        return { name: k, id: config.stations[k] }
      });
    });
    //  this.configService.getConfParameter('tfl-bike').subscribe( data => {
    //   const stationIDs = (data['tfl-bike'] && data['tfl-bike']['stations']) ? data['tfl-bike']['stations'] : [];
    //   if (data['tfl-bike'] && data['tfl-bike']['stations']) {
    //     this.stationids = Object.keys(stationIDs).map((k) => {
    //       return {name: k, id: stationIDs[k]}
    //     });
    //   }
    // });
    // this.stationids = [
    //   {name: 'Limehouse', id: 'BikePoints_542'},
    //   {name: 'Barbican', id: 'BikePoints_275'}
    // ];
    this.interval = 60000; // secondes
  }


  ngOnInit() {
    console.log('Init TFL BIKEPOINT');

    this.getBornesData();

    setInterval(() => {
      this.getBornesData();
    }, this.interval);
  }

  getBornesData() {
    this.stations = [];
    if (this.stationids) {
      Observable
        .from(this.stationids)
        .flatMap((i) => this.tflbikepointService.getBikeData(i))
        .subscribe((o) => {
          const stationObject = {
            name: o.commonName,
            bikes: !!(o.additionalProperties.length) ? o.additionalProperties[6].value : '',
            attachs: !!(o.additionalProperties.length) ? o.additionalProperties[7].value : ''
          };
          this.stations.push(stationObject);
        });
    }
  }

}
