import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TflBikepointComponent } from './tfl-bikepoint.component';
import { TflBikepointService } from './tfl-bikepoint.service';
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    TflBikepointComponent
    ],
    exports: [
      TflBikepointComponent
    ],
    providers: [
      TflBikepointService
    ]
})
export class TflBikepointModule {

 }
