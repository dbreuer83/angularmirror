import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
let ison = null;
@Component({
  selector: 'hall-9000',
  template: `<div class="wrap">
		<div class="inner">
			<div class="hal9000">
				<div class="eye" [class.active]="isSpeaking"></div>
				<div class="reflection"></div>
			</div>
		</div>
	</div>`,
  styleUrls: ['./hall.min.css']
})
export class HallComponent implements OnInit  {
  public isSpeaking = false;

  ngOnInit() {

  }
}
