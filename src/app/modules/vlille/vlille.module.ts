import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VlilleComponent } from './vlille.component';
import { VlilleService } from './vlille.service';
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    VlilleComponent
    ],
    exports: [
      VlilleComponent
    ],
    providers:[
      VlilleService
    ]
})
export class VlilleModule {

 }
