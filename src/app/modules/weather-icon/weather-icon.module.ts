import { WeatherService } from './../weather/weather.service';
import { SharedModule } from '../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeatherIconComponent } from './weather-icon.component';
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [
    WeatherIconComponent
    ],
    exports: [
      WeatherIconComponent
    ],
    providers: [
      WeatherService
    ]
})
export class WeatherIconModule {

 }
