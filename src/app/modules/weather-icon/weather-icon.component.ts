import { ConfigService } from './../../config/config.service';
import { Component, OnInit, HostBinding } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';

import { WeatherService } from '../weather/weather.service';
// import { FirstletterupperPipe } from '../../pipes/firstletterupper.pipe';

@Component({
  selector: 'weather-icon',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './weather-icon.component.html',
  styleUrls: ['./weather-icon.component.scss']
})

export class WeatherIconComponent implements OnInit {
  @HostBinding('class') modulePosition: string;
  public isActive = false;

  public current_weather_type: String = '';
  public weather_types: Array<Object> = [];
  public weather_object: any;
  public weather_city: string;
  public weather_temp: number = 0;
  public weather_all: any;

  constructor(public weatherService: WeatherService, private configService: ConfigService) {
    this.weather_types.push('sun-shower');
    this.weather_types.push('thunder-storm');
    this.weather_types.push('cloudy');
    this.weather_types.push('sunny');
    this.weather_types.push('rainy');
    this.weather_types.push('flurries');
    this.weather_types.push('haze');
    configService.getConfParameter('currentweather').subscribe((config: any) => {
      this.modulePosition = config.position;
      this.isActive = config.active;
      this.getWeather();
    });
  }

  ngOnInit() {
    console.log('Init Weather icon');
    this.getWeather();
    setInterval(() => {
      this.getWeather();
    }, 60 * 1000 * 5); // 5 min
  }

  getWeather() {
    console.log("Get weather Icon", this.isActive);
    if (!this.isActive) {
      return false;
    }
    this.weatherService.getCurrent()
      .subscribe(res => {
        this.modulePosition = res.position;
        this.weather_temp = Math.round(res.main.temp);
        this.weather_all = res.weather[0];
        this.weather_city = res.name + ', ' + res.sys.country;
        this.setWeatherType(res.weather[0].main);
      });
  }

  randomType() {
    const randomInt = Math.floor(Math.random() * this.weather_types.length);
    return this.weather_types[randomInt];
  }

  setWeatherType(type) {
    if (!type) {
      type = this.randomType();
    }
    this.current_weather_type = type;
  }

}
