import { SharedModule } from '../../shared/shared.module';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarComponent } from './car.component';
import { CarService } from './car.service';
// import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [
    CarComponent
  ],
  exports: [
    CarComponent
  ],
  providers: [CarService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CarModule {

}
