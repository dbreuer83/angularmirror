import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class CarService {

  public api_url: String = '';

  constructor(public http: Http) {
    // this.api_url = apiService.getUrl('xee');
  }

  getCarData(data) {
    return this.http
      .get(this.api_url + data)
      .map(res => res.json());
  }
}
