import { ConfigService } from './../../config/config.service';
import { Component, OnInit, ViewEncapsulation, ChangeDetectorRef, HostBinding } from '@angular/core';

@Component({
  selector: 'mm-compliments',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './compliments.component.html',
  styleUrls: ['./compliments.component.scss']
})
export class ComplimentsComponent implements OnInit {
  @HostBinding('class') modulePosition: string;
  public isActive = false;
  public config: any = {
    compliments: {
      morning: [
        'Good morning, handsome!',
        'Enjoy your day!',
        'How was your sleep?'
      ],
      afternoon: [
        'Hello, beauty!',
        'You look sexy!',
        'Looking good today!'
      ],
      evening: [
        'Wow, you look hot!',
        'You look nice!',
        'Hi, sexy!'
      ]
    },
    updateInterval: 30000,
    remoteFile: null,
    fadeSpeed: 4000
  }

  lastComplimentIndex: number = -1;
  private compliments: Array<String>;
  public sayCompliment: String;


  constructor(private cdRef: ChangeDetectorRef, private configService: ConfigService) {
    configService.getConfParameter('compliments').subscribe((config: any) => {
      this.modulePosition = config.position;
      this.isActive = config.active;
    });
  }

  ngOnInit() {
    console.log('Init Compliments');
    setInterval(() => {
      this.updateCompliments();
    }, 3000);

  }

  updateCompliments() {
    let hour = new Date().getHours();

    if (hour >= 3 && hour < 12) {
      this.compliments = this.config.compliments.morning;
    } else if (hour >= 12 && hour < 17) {
      this.compliments = this.config.compliments.afternoon;
    } else {
      this.compliments = this.config.compliments.evening;
    }
    const cindex = Math.floor((Math.random() * this.compliments.length) + 0);
    this.sayCompliment = this.compliments[cindex];
  }

}
