import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComplimentsComponent } from './compliments.component';
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ComplimentsComponent
    ],
    exports: [
      ComplimentsComponent
    ]
})
export class ComplimentsModule {

 }
