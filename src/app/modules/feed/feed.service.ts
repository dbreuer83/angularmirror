import { Feed } from './feed.model';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

/**
* This class provides the Feed service with methods to read names and add names.
*/
@Injectable()
export class FeedService {
   private rssToJsonServiceBaseUrl: string = 'https://rss2json.com/api.json?rss_url=';
/**
* Creates a new FeedService with the injected Http.
* @param {Http} http - The injected Http.
* @constructor
*/
  constructor(private http: Http) {}
/**
* Returns an Observable for the HTTP GET request for the JSON resource.
* @return {string[]} The Observable for the HTTP request.
*/
  getFeedContent(url: string): Observable<Feed> {
    return this.http.get(this.rssToJsonServiceBaseUrl + url)
    .map(this.extractFeeds)
    //              .do(data => console.log('server data:', data))  // debug
    .catch(this.handleError);
  }


  /**
   *
   * @param res {Response} feed response
   */
  private extractFeeds(res: Response): Feed {
    let feed = res.json();
    return feed || { };
  }

/**
* Handle HTTP error
*/
  private handleError (error: any) {
    let errMsg = (error.message) ? error.message :
    error.status ? `error.status - error.statusText` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
}
