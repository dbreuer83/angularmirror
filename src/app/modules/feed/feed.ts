import { ConfigService } from './../../config/config.service';
import { StripHtmlTagsPipe } from './strip-html-tags.pipe';
import { FeedCardComponent } from './feed.card';
import { FeedEntry } from './feed.model';
import { FeedService } from './feed.service';
import { Component, OnInit,OnDestroy, HostBinding, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MdCardModule } from '@angular/material';
import { MdToolbarModule } from '@angular/material';
import { MdButtonModule } from '@angular/material';
import { MdIconModule, MdIconRegistry } from '@angular/material';


@Component({
  selector: 'news-feed',
  template: `<app-feed-card *ngIf="currentfeeds && isActive" [feed]="currentfeeds" ></app-feed-card>`,
})
export class NewsFeedComponent implements OnInit, OnDestroy {
  @HostBinding('class') modulePosition: string;
  public isActive = false;

  private feedUrl = 'http://feeds.bbci.co.uk/news/uk/rss.xml?edition=uk';
  private feeds: Array<FeedEntry> = [];
  public currentfeeds: FeedEntry;
  public currentFeedIndex: number = 0;

  public feedInterval = null;

  constructor(private feedService: FeedService, private configService: ConfigService) {
    configService.getConfParameter('newsfeed').subscribe((config: any) => {
      this.modulePosition = config.position;
      this.isActive = config.active;
      this.feedUrl = config['config']['feeds'][0]['url'] || 'http://feeds.bbci.co.uk/news/uk/rss.xml?edition=uk';
      this.refreshFeed();
    });

  }

  ngOnInit() {
    console.log('Init News Feed');
    clearInterval(this.feedInterval);
    this.feedInterval = setInterval(() => {
      this.refreshFeed();
    }, 60000);
  }

  ngOnDestroy() {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    clearInterval(this.feedInterval);
  }

  feedLoop() {
    this.currentFeedIndex++;
    // Adds 1s of delay to provide user's feedback.
    if (this.currentFeedIndex > this.feeds.length) {
      this.currentFeedIndex = 0;
    }
    this.currentfeeds = this.feeds[this.currentFeedIndex];
  }

  private refreshFeed() {
    this.feedService.getFeedContent(this.feedUrl).delay(1000)
      .subscribe(
      feed => {
        // console.log(feed.items[this.currentFeedIndex]);
        this.feeds = feed.items;
        this.currentFeedIndex = 0;
        clearInterval(this.feedInterval);
        this.feedInterval = setInterval( () => {
          this.feedLoop();
        }, 7000);
      },
      error => console.log(error));
  }
}




const MD_MODULES = [
  MdCardModule,
  MdToolbarModule,
  MdButtonModule,
  MdIconModule
]

@NgModule({
  imports: [
    CommonModule,
    ...MD_MODULES
  ],
  declarations: [NewsFeedComponent, FeedCardComponent, StripHtmlTagsPipe],
  entryComponents: [NewsFeedComponent, FeedCardComponent],
  exports: [NewsFeedComponent, FeedCardComponent, ...MD_MODULES, StripHtmlTagsPipe],
  providers: [FeedService, MdIconRegistry]
})
export class FeedModule { }
