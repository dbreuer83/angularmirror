import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-feed-card',
  template: `
  <md-card class="card-feed-entry" (click)="openLinkInBrowser()">
    <md-card-title>{{feed.title}}</md-card-title>
    <md-card-content>{{feed.description | stripHtmlTags}}</md-card-content>
    <md-card-actions align="end">
      <button md-button><img src="assets/feed/bbcnews.png" width="100" [alt]="this.feed.title" [title]="this.feed.title" /></button>
    </md-card-actions>
  </md-card>
  `,
  styles: [`
    .card-feed-entry {
      margin: 10px 0;
    }
    .mat-card {
      background: rgba(255, 255, 255, 0.4);
      max-width: calc(100vw - 300px);
    }
  `]
})
export class FeedCardComponent implements OnInit {

  @Input() feed: any;

  constructor() { }

  ngOnInit() {
  }

  public openLinkInBrowser() {
    window.open(this.feed.link);
  }
}
