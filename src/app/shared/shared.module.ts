import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  AddZeroPipe,
  XmlParsePipe,
  FirstletterupperPipe
} from '../pipes';


export const PIPES = [
  AddZeroPipe,
  XmlParsePipe,
  FirstletterupperPipe
];

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ...PIPES
  ],
  exports: [
    ...PIPES
  ]
})
export class SharedModule { }
