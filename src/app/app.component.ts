import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ConfigService } from './config/config.service';

@Component({
  selector: 'app-root',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  title = 'app works!';
  deviceUUID: any;
  isAuth = false;
  conf: any;
  confChanged = false;
  modules: any = [
    'datetime',
    'message'
  ];

    public uuid = {
    s4: () => {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    },
    get: ():string => {
      if (localStorage && localStorage.getItem('remoteID') !== null) {
        this.isAuth = false;
        return localStorage.getItem('remoteID');
      } else {
        let remoteID = this.uuid.s4() + this.uuid.s4() + '-' + this.uuid.s4() + '-'
        + this.uuid.s4() + '-' + this.uuid.s4() + '-' + this.uuid.s4() + this.uuid.s4()
        + this.uuid.s4();
        localStorage.setItem('remoteID', remoteID);
        this.isAuth = true;
        return remoteID;
      }
    }
  }

  constructor(
    public configService: ConfigService) {
    this.deviceUUID = this.uuid.get();
  }

  ngOnInit() {
    this.conf = this.configService.getConf();
      // .subscribe(res => {
      //   // console.log(res);
      //   if (res.reload) {
      //     this.conf = res;
      //     // this.refreshPage(res.reload * 1000);
      //   }
      // });
  }

  refreshPage(reload) {
    setTimeout(() => {
      // console.log(reload);
      localStorage.setItem("confChanged", "true");
      console.log("on reload la page :)");
      window.location.reload();
    }, reload);
  }

  getConfParameter(param) {
    // console.log(param);
    // return this.configService.getConfParameter(param)
    //   .subscribe(res => { res; });
  }






}
