import { HallModule } from './modules/hall-9000/hall.module';
import { FeedModule } from './modules/feed/feed';
import { RemoteModule } from './modules/remote/remote.module';
import { ConstantsService } from './api/constants.service';
import { TweetlineModule } from './modules/tweetline/tweetline.module';
import { ConfigService } from './config/config.service';

import { SharedModule } from './shared/shared.module';
import { WeatherIconModule } from './modules/weather-icon/weather-icon.module';
import { WeatherModule } from './modules/weather/weather.module';
import { VlilleModule } from './modules/vlille/vlille.module';
import { TwitterModule } from './modules/twitter/twitter.module';
import { MessageModule } from './modules/message/message.module';
import { HomeModule } from './modules/home/home.module';
// import { HomeComponent } from './modules/home/home.component';
import { DateTimeModule } from './modules/datetime/datetime.module';
import { CarModule } from './modules/car/car.module';
import { ClockModule } from './modules/clock/clock.module';
import { SocialModule } from './modules/social/social.module';
import { CalendarModule } from './modules/calendar/calendar.module';
import { TflBikepointModule } from './modules/tfl-bikepoint/tfl-bikepoint.module';
import { BubiBikepointModule } from './modules/bubi-bikepoint';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations'
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {
  ComplimentsModule
} from './modules';



export const MODULES = [
  CalendarModule,
  CarModule,
  ClockModule,
  ComplimentsModule,
  DateTimeModule,
  HomeModule,
  MessageModule,
  SocialModule,
  TflBikepointModule,
  BubiBikepointModule,
  TwitterModule,
  TweetlineModule,
  VlilleModule,
  WeatherModule,
  WeatherIconModule,
  RemoteModule,
  FeedModule,
  HallModule
];

import { AngularFireModule } from 'angularfire2';
import { MasterComponent } from './core/core.component';


export const firebaseConfig = {
    apiKey: 'AIzaSyAzcMWw0mb1a_bSOMWf--ODmgGbIhhy1vA',
    authDomain: 'angular-mirror.firebaseapp.com',
    databaseURL: 'https://angular-mirror.firebaseio.com',
    projectId: 'angular-mirror',
    storageBucket: 'angular-mirror.appspot.com',
    messagingSenderId: '747305752795'
};


@NgModule({
  declarations: [
    AppComponent,
    MasterComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    SharedModule,
    ...MODULES,
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [ ConfigService, ConstantsService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }

